#stage builder
FROM golang:latest as builder
LABEL maintainer="Hugo Nijhuis-Mekkelholt <hugo@nijmek.com>"

WORKDIR /app
COPY go.mod ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

#stage runtime
FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /app/main .

EXPOSE 5000

CMD ["./main"] 